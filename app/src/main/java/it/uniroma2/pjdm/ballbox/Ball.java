package it.uniroma2.pjdm.ballbox;

import android.content.Context;
import android.graphics.Point;
import android.util.Log;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import java.util.Random;


public class Ball {
    private ImageView iv;   /* the ball image */
    private int color;      /* the ball color */
    private int x;          /* the ball x coordinate */
    private int y;          /* the ball y coordinate */
    private Context ctx;    /* the activity context */

    /* color codes */
    public final static int COLOR_WHITE = 0;
    public final static int COLOR_RED= 1;
    public final static int COLOR_GREEN = 2;
    public final static int COLOR_BLUE = 3;

    /* Constructor */
    public Ball(Context context, ImageView imageView) {
        this.ctx = context;
        this.iv = imageView;
    }

    /* Set the position of the ball */
    public void setPosition(int x, int y) {
        FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(
            new ViewGroup.MarginLayoutParams(
                    FrameLayout.LayoutParams.WRAP_CONTENT,
                    FrameLayout.LayoutParams.WRAP_CONTENT
            )
        );
        lp.setMargins(x, y, 0, 0);
        iv.setLayoutParams(lp);
        this.x = x;
        this.y = y;
    }

    /* Set the color of the ball */
    public void setColor(int color) {
        this.color = color;
        switch(color) {
            case COLOR_WHITE:
                iv.setImageResource(R.drawable.ball_white);
                break;
            case COLOR_RED:
                iv.setImageResource(R.drawable.ball_red);
                break;
            case COLOR_GREEN:
                iv.setImageResource(R.drawable.ball_green);
                break;
            default:
                iv.setImageResource(R.drawable.ball_blue);
        }
    }

    /* private method to get random x and y coordinates */
    private Point getRandomPosition() {
        Point size = new Point();
        Point result = new Point();
        WindowManager wm = (WindowManager) ctx.getSystemService(Context.WINDOW_SERVICE);
        wm.getDefaultDisplay().getSize(size);
        int ballx = new Random().nextInt(size.x  / 3) + size.x / 4;
        int bally = new Random().nextInt(size.y  / 3) + size.y / 4;
        Log.d("CLA", "display size: " + size.x + "x" + size.y + " chosen: " + ballx + "," + bally);
        result.set(ballx, bally);
        return result;
    }

    /* set the ball in a random position on the screen */
    public void setRandomPosition() {
        Point position = getRandomPosition();
        setPosition(position.x, position.y);
    }

    /* set the ball to a random color */
    public void setRandomColor() {
        int color = new Random().nextInt(4);
        setColor(color);
    }

    /* when we need a new ball, we reuse the existing one,
       changing position and color */
    public void renew() {
        /* position the ball randomly */
        setRandomPosition();

        /* choose a color for the ball */
        setRandomColor();
    }

    /*getters */

    public int getColor() {
        return color;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public ImageView getImageView() {
        return iv;
    }
}
