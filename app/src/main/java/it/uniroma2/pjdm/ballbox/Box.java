package it.uniroma2.pjdm.ballbox;

import android.content.Context;
import android.widget.ImageView;

/**
 * Created by clauz on 8/22/18.
 */

public class Box {
    private ImageView iv;
    private int color;

    public Box(ImageView imageView) {
        this.iv = imageView;
    }

    public void setColor(int color) {
        this.color = color;
        switch (color) {
            case Ball.COLOR_WHITE:
                iv.setImageResource(R.drawable.ballbox_white);
                break;
            case Ball.COLOR_RED:
                iv.setImageResource(R.drawable.ballbox_red);
                break;
            case Ball.COLOR_GREEN:
                iv.setImageResource(R.drawable.ballbox_green);
                break;
            default:
                iv.setImageResource(R.drawable.ballbox_blue);
        }
    }

    public int getColor() {
        return color;
    }

    public ImageView getImageView() {
        return iv;
    }

    public void swapColors(Box that) {
        int tmpColor = this.getColor();
        this.setColor(that.getColor());
        that.setColor(tmpColor);
    }
}
