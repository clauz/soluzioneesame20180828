package it.uniroma2.pjdm.ballbox;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;

import java.util.Random;

public class MainActivity extends AppCompatActivity {

    private Ball ball;
    private Box[] boxes = new Box[4];
    private boolean grabbed = false;
    private int offset_x;
    private int offset_y;
    private int nballs;

    private final static String KEY_CHOSENCOLOR = "CHOSENCOLOR";
    private final static String KEY_NBALLS = "NBALLS";


    private void increaseAndSwapBoxes() {
        /* increase the balls counter */
        nballs++;

        if(nballs % 10 == 0) {
            /* choose two random boxes */
            int index1 = new Random().nextInt(boxes.length);
            int index2 = index1;

            while (index2 == index1)
                index2 = new Random().nextInt(boxes.length);

            /* swap their colors */
            boxes[index1].swapColors(boxes[index2]);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putInt(KEY_CHOSENCOLOR, ball.getColor());
        outState.putInt(KEY_NBALLS, nballs);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        /* the boxes */
        boxes[0] = new Box((ImageView) findViewById(R.id.box1));
        boxes[1] = new Box((ImageView) findViewById(R.id.box2));
        boxes[2] = new Box((ImageView) findViewById(R.id.box3));
        boxes[3] = new Box((ImageView) findViewById(R.id.box4));

        boxes[0].setColor(Ball.COLOR_WHITE);
        boxes[1].setColor(Ball.COLOR_RED);
        boxes[2].setColor(Ball.COLOR_GREEN);
        boxes[3].setColor(Ball.COLOR_BLUE);

        nballs = 0;

        /* the ball */
        ball = new Ball(getApplicationContext(), (ImageView)findViewById(R.id.ball));
        ball.getImageView().setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    if (motionEvent.getActionMasked() == MotionEvent.ACTION_DOWN) {
                        offset_x = (int) motionEvent.getX();
                        offset_y = (int) motionEvent.getY();
                        grabbed = true;
                    }
                    return false;
                }
            });

        /* recover the saved instance state */
        if(savedInstanceState != null && savedInstanceState.containsKey(KEY_CHOSENCOLOR)) {
            ball.setRandomPosition();
            int chosencolor = savedInstanceState.getInt(KEY_CHOSENCOLOR);
            ball.setColor(chosencolor);
        } else {
            ball.renew();
            increaseAndSwapBoxes();
        }

        if(savedInstanceState != null && savedInstanceState.containsKey(KEY_NBALLS)) {
            this.nballs = savedInstanceState.getInt(KEY_NBALLS);
        }

        /* drag logic */
        FrameLayout flayout = findViewById(R.id.flayout);
        flayout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if(!grabbed)
                    return false;

                Log.d("CLA", "Action: " + motionEvent.getAction());

                if(motionEvent.getAction() == MotionEvent.ACTION_MOVE) {
                    int x = (int) motionEvent.getX() - offset_x;
                    int y = (int) motionEvent.getY() - offset_y;

                    int maxwidth = view.getWidth() - ball.getImageView().getWidth();
                    int maxheight = view.getHeight() - ball.getImageView().getHeight();

                    if(x > maxwidth)
                        x = maxwidth;
                    if(x < 0)
                        x = 0;
                    if(y > maxheight)
                        y = maxheight;
                    if(y < 0)
                        y = 0;

                    Log.d("CLA", "x: " + x + "/" + maxwidth + " y: " + y + "/" + maxheight);

                    ball.setPosition(x, y);

                } else if(motionEvent.getAction() == MotionEvent.ACTION_UP ||
                        motionEvent.getAction() == MotionEvent.ACTION_CANCEL) {
                    Log.d("CLA", "ungrab");
                    grabbed = false;
                }


                Log.d("CLA", "box1: " +
                          boxes[0].getImageView().getRight() +
                        " " + boxes[0].getImageView().getLeft() +
                        " " + boxes[0].getImageView().getTop() +
                        " " + boxes[0].getImageView().getBottom());

                Log.d("CLA", "box4: " +
                        boxes[3].getImageView().getRight() +
                        " " + boxes[3].getImageView().getLeft() +
                        " " + boxes[3].getImageView().getTop() +
                        " " + boxes[3].getImageView().getBottom());

                Log.d("CLA", "ball: " +
                        (ball.getX() + ball.getImageView().getWidth()) +
                        " " + ball.getX() +
                        " " + ball.getY() +
                        " " + (ball.getY() + ball.getImageView().getHeight()));

                /* check if we are in the right box */
                Box rightBox = boxes[0];
                for(Box b: boxes) {
                    if(b.getColor() == ball.getColor()) {
                        rightBox = b;
                        break;
                    }
                }

                ImageView box = rightBox.getImageView();
                if( box.getRight() >= ball.getX() + ball.getImageView().getWidth() &&
                        box.getLeft() <= ball.getX() &&
                        box.getTop() <= ball.getY() &&
                        box.getBottom() >= ball.getY() + ball.getImageView().getHeight()) {
                    Log.d("CLA", "ball in box");
                    grabbed = false;
                    ball.renew();
                    increaseAndSwapBoxes();
                }

                return true;
            }
        });

    }
}
